import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {UserComponent} from './components/user/user.component';
import {LoginComponent} from './components/login/login.component';
import {AddNewComponent} from './components/user/add-new/add-new.component';
import {EditComponent} from './components/user/edit/edit.component';
import {RegistateComponent} from './components/register/registate.component';
import {RegisterGuard} from './guards/register.guard';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'register', component: RegistateComponent, canActivate: [RegisterGuard]},
  {
    path: 'users',
    component: UserComponent,
    data: {preload: true},
    children: [
      {
        path: 'add',
        component: AddNewComponent
      },
      {
        path: 'edit/:id',
        component: EditComponent
      }
    ]
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
