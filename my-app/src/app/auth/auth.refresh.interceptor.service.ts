import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthRefreshInterceptor implements HttpInterceptor {
  constructor(
    public Auth: AuthService,
    private router: Router
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError((err: HttpErrorResponse  ) => {
        if (err.status === 401) {
          this.Auth.setTokens(err.error.accessToken, err.error.refreshToken);
          return next.handle(req);
        } else if (err.status === 403) {
          this.router.navigate(['']);
        }

      })
    );
  }
}
