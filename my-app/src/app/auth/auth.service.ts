import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public getAccessToken(): string {
    return localStorage.getItem('accessToken');
  }
  public getRefreshToken(): string {
    return localStorage.getItem('refreshToken');
  }

  public setTokens(access, refresh): any {
    localStorage.setItem('accessToken', access);
    localStorage.setItem('refreshToken', refresh);
  }
  constructor() { }
}
