import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http: HttpClient
  ) { }

  sendToGetToken(formValue): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })};
    return this.http.post('http://localhost:3000/login', formValue, options);
  }
}

