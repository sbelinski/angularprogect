import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {LoginService} from './login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  loginForm = new FormGroup({
    login: new FormControl(),
    password: new FormControl(),
  });

  sendRequest() {
    this.loginService.sendToGetToken(this.loginForm.value).subscribe(data => {
      localStorage.setItem('accessToken', data.accessToken);
      localStorage.setItem('refreshToken', data.refreshToken);
      this.router.navigate(['users']);
    });
  }

  ngOnInit() {
  }

}
