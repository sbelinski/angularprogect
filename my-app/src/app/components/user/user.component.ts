import {Component, Input, OnInit} from '@angular/core';
import {UserService} from './user.service';
import {UserInterface} from './user.interface';
import {SharedAddingService} from './add-new/shared.adding.service';
import {SharedEditingService} from './edit/shared.editing.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
  users: UserInterface[];
  id: string;
  constructor( private userService: UserService,
               private sharedEditingService: SharedEditingService,
               private sharedAddingService: SharedAddingService) { }
  deleteOne(id) {
      this.userService.deleteOneUser(id).subscribe(data => {this.users = data; });
  }

  pushOne(user) {
    this.userService.postOneUser(user).subscribe(data => {this.users.push(data); });
  }

  updateOne(user) {
      this.userService.updateOneUser(user).subscribe(data => {});
      this.userService.getUsersAll().subscribe(date => this.users = date);
  }
 ngOnInit() {
      this.userService.getUsersAll().subscribe(data => this.users = data);
      this.sharedAddingService.initOne();
      this.sharedAddingService.returnObservable().subscribe(data => this.pushOne(data));

      this.sharedEditingService.initOne();
      this.sharedEditingService.returnObservable().subscribe(data => this.updateOne(data));
  }
}
