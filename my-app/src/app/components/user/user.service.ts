import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  getUsersAll(): Observable<any> {
     return this.http.get('http://localhost:3000/user');
  }

  postOneUser(user): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };
    return this.http.post('http://localhost:3000/user', user, options);
  }

  updateOneUser(user): Observable<any> {
    return this.http.put('http://localhost:3000/user', user);
  }

  getUser(id): Observable<any> {
    return this.http.get(`http://localhost:3000/user/${id}`);
  }
  deleteOneUser(id): Observable<any> {
    const url = `http://localhost:3000/user/${id}`;
    return this.http.delete(url);
  }
}
