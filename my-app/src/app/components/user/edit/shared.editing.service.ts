import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedEditingService {
  newUser$: Observable<object> ;
  newUser: Subject<object>;

  initOne(): void {
    this.newUser = new Subject();
    this.newUser$ = this.newUser.asObservable();
  }

  returnObservable(): Observable<object> {
    return this.newUser$;
  }

  updateOne(user) {
    this.newUser.next(user);
  }
  constructor() { }
}
