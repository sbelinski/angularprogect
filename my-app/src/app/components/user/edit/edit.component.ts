import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import {FormControl, FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {SharedEditingService} from './shared.editing.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  users: any[];
  id: string;
  constructor(  private readonly userService: UserService,
                private sharedEditingService: SharedEditingService,
                private readonly route: ActivatedRoute) { }

  formGroup = new FormGroup({
    firstName : new FormControl(''),
    lastName : new FormControl(''),
    age : new FormControl(''),
    email : new FormControl(''),
});
  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.getOneUser(this.id);
  }

  getOneUser(id): void {
    this.userService.getUser(id).subscribe(data => this.formGroup.patchValue(data));
  }

  updateOne(): void {
    const req = this.formGroup.value;
    req._id = this.route.snapshot.params.id;
    this.sharedEditingService.updateOne(req);
  }
}
