import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserComponent} from './user.component';
import {UserService} from './user.service';
import { EditComponent } from './edit/edit.component';
import { AddNewComponent } from './add-new/add-new.component';


@NgModule({
  declarations: [UserComponent, EditComponent, AddNewComponent, EditComponent],
  imports: [
    CommonModule,
    UserService
  ]
})
export class UserModule { }
