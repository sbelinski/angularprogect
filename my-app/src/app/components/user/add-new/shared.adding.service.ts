import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedAddingService {
newUser$: Observable<object> ;
newUser: Subject<object>;

  initOne() {
    this.newUser = new Subject();
    this.newUser$ = this.newUser.asObservable();
  }

  returnObservable(): Observable<object> {
    return this.newUser$;
  }

  postOne(user): void {
    this.newUser.next(user);
  }
  constructor() { }
}
