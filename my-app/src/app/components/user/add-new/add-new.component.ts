import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {SharedAddingService} from './shared.adding.service';

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.css']
}
)
export class AddNewComponent implements OnInit {

  constructor( private readonly sharedService: SharedAddingService) { }

  formGroup = new FormGroup({
    firstName : new FormControl(''),
    lastName : new FormControl(''),
    age : new FormControl(''),
    email : new FormControl(''),
  });

  postOne(): void {
    this.sharedService.postOne(this.formGroup.value);
  }

  ngOnInit() {
  }

}
