import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {RegisterService} from './register.service';

@Component({
  selector: 'app-registate',
  templateUrl: './registate.component.html',
  styleUrls: ['./registate.component.css']
})
export class RegistateComponent implements OnInit {

  constructor(
    private regiesterService: RegisterService
  ) { }

  registrationForm = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    age: new FormControl(),
    login: new FormControl(),
    password: new FormControl(),
  });


  register(): void {
    const req = this.registrationForm.value;
    this.regiesterService.registerOneUser(req).subscribe(data => console.log(data));
  }

ngOnInit() {
  }

}
