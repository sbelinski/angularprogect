import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-preloader',
  templateUrl: './preloader.component.html',
  styleUrls: ['./preloader.component.css']
})
export class PRELOADERComponent implements OnInit {

  constructor() { }

  @Input() loading: false;

  ngOnInit() {
  }
}
