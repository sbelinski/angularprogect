import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import {UserComponent} from './components/user/user.component';
import { MzButtonModule, MzInputModule } from 'ngx-materialize';
import {HttpClientModule} from '@angular/common/http';
import {AddNewComponent} from './components/user/add-new/add-new.component';
import {EditComponent} from './components/user/edit/edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RegistateComponent } from './components/register/registate.component';
import {RegisterGuard} from './guards/register.guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {AuthInterceptor} from './auth/auth.interceptor.service';
import {AuthRefreshInterceptor} from './auth/auth.refresh.interceptor.service';
import { PRELOADERComponent } from './components/preloader/preloader.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserComponent,
    AddNewComponent,
    EditComponent,
    RegistateComponent,
    PRELOADERComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    MzButtonModule,
    MzInputModule
  ],
  providers: [RegisterGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthRefreshInterceptor, multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
